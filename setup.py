#!/usr/bin/python
# -*- coding: utf-8 -*-

try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

import imp
import os
irklab = imp.load_source('irklab',
                         os.path.join(os.path.dirname(__file__), 'irklab'))
setup(name='irklab',
      description='IRC gitlab gateway',
      long_description=irklab.__doc__,
      version=irklab.__version__,
      url='https://gitlab.com/anarcat/irklab',
      author=irklab.__author__,
      author_email=irklab.__email__,
      license='AGPL3',
      keywords='irc gitlab git hook bot',
      scripts=['irklab'],
      )
